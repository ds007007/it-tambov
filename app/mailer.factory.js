var app = angular.module("ItTambov");

app.factory("Mailer", function($http){
    return {
      sendEmail: function(msg){

        var fromEmail = 'info@itcaring.ru';
        var fromName = 'Телекоммуникационные услуги в Тамбове';
        var replyTo = 'email';

        //delete default content-type in header
        delete $http.defaults.headers.common['X-Requested-With']

        return $http.post('https://mandrillapp.com/api/1.0//messages/send.json', {
                      'key': 'gN8wi49bBEP7M0JDG4qDaA',
                      'message': {
                          'html': msg,
                          'subject': "Новая заявка с сайта itcaring.ru",
                          'from_email': fromEmail,
                          'from_name': fromName,
                          'to': [
                              {
                                  'email': "info@itcaring.ru",
                                  'type': 'to'
                              }
                          ]
                      }
                  })
                  .success(function(data, status, headers, config){
                    
                  });

      }
    };
  }
);