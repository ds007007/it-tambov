'use strict';

var app = angular.module("ItTambov");

app.directive("slider", function(){
  return {
    scope: {
      onChange: "&"
    },
    restrict: 'AE',
    link: function(scope, elem, attrs){
      //Get attributes
      var min = parseInt(attrs.min || 1), 
          max = parseInt(attrs.max || 10), 
          step = parseInt(attrs.step || 1),
          pips = parseInt(attrs.pips || 1),
          value = parseInt(attrs.value || min);

      //console.log(pips);

      elem.ready(function(){
        //Build slider
        elem.slider({
          range: 'max',
          min: min,
          max: max,
          step: step,
          change: function(event, ui){
            scope.onChange({change: elem.slider("value")});
          }
        }).slider("pips", { step: pips}).slider("float").slider("value", value);
        
      });
    }
  }
});