'use strict';

var app = angular.module("ItTambov", ["ui.mask"]);

app.controller("MainController", function ($scope, $http, Mailer){

  /*  TotalPrice Property*/ 
  Object.defineProperty($scope, "totalPrice", {
    writeable: false,
    get: function(){
      return getTotalPrice();
    }
  })
  /*  End TotalPrice Property*/ 

  $scope.phoneMask = "+7(999)999-99-99";

  $scope.newOrderData = {
    fio: "",
    status: "private",
    phone: "",
    email: "",
    msg: ""
  };

  $scope.compUnitPrice = 0;

  $scope.baseTariffPrice = {};
  $scope.standardTariffPrice = {};
  $scope.currentTariffPrice = {};
  $scope.customerTariffOptions = {
    comp: 1,
    org: 0,
    soho: 0,
    winServer: 0,
    miniATS: 0,
    localNetwork: false
  };

  //Tariff price components
  $scope.sComp = "";
  $scope.sOrg = "";
  $scope.sSoho = "";
  $scope.sWinServer = "";
  $scope.sMiniATS = "";
  $scope.sLocalNetwork = "";

  //Get tariffs
  $http.get('tariffs/base.json').success(function(data, status, headers, config){
    $scope.baseTariffPrice = data;
    $scope.currentTariffPrice = $scope.baseTariffPrice;

    setCompUnitCount();
    setTariffPriceComponents();

    setTimeout(function(){
      updateView();
    });
  });
  $http.get('tariffs/standard.json').success(function(data, status, headers, config){
    $scope.standardTariffPrice = data;
  });


  //Current tariff changed by tab selection
  $scope.setBaseTariff = function(){
    $scope.currentTariffPrice = $scope.baseTariffPrice;

    setCompUnitCount();
    updateView();
  }
  $scope.setStandardTariff = function(){
    $scope.currentTariffPrice = $scope.standardTariffPrice;

    setCompUnitCount();
    updateView();
  }


  //Customer tariff options changed
  $scope.compCountChanged = function(change){
    $scope.customerTariffOptions.comp = change;

    setCompUnitCount();
    updateView();
    $scope.$apply();
  }
  $scope.orgCountChanged = function(change){
    $scope.customerTariffOptions.org = change;

    updateView();
    $scope.$apply();
  }
  $scope.sohoCountChanged = function(change){
    $scope.customerTariffOptions.soho = change;

    updateView();
    $scope.$apply();
  }
  $scope.winserverCountChanged = function(change){
    $scope.customerTariffOptions.winServer = change;

    updateView();
    $scope.$apply();
  }
  $scope.atsCountChanged = function(change){
    $scope.customerTariffOptions.miniATS = change;

    updateView();
    $scope.$apply();
  }
  $scope.checkLocalNetwork = function(){
    $scope.customerTariffOptions.localNetwork = !$scope.customerTariffOptions.localNetwork;

    updateView();
  }

  //Comp unit price value above slider
  var setCompUnitCount = function(){
    if (!$scope.currentTariffPrice.comp) return;//when page wasn't loaded

    if ($scope.customerTariffOptions.comp >=1 && $scope.customerTariffOptions.comp <=5)
      $scope.compUnitPrice = $scope.currentTariffPrice.comp.comp_1_5;
    else if ($scope.customerTariffOptions.comp >=6 && $scope.customerTariffOptions.comp <=10)
      $scope.compUnitPrice = $scope.currentTariffPrice.comp.comp_6_10;
    else if ($scope.customerTariffOptions.comp >=11 && $scope.customerTariffOptions.comp <=15)
      $scope.compUnitPrice = $scope.currentTariffPrice.comp.comp_11_15;
    else if ($scope.customerTariffOptions.comp >=16 && $scope.customerTariffOptions.comp <=24)
      $scope.compUnitPrice = $scope.currentTariffPrice.comp.comp_16_24;
    else if ($scope.customerTariffOptions.comp >=25 && $scope.customerTariffOptions.comp <=40)
      $scope.compUnitPrice = $scope.currentTariffPrice.comp.comp_25_40;
  }

  var setTariffPriceComponents = function(){
    if (!$scope.currentTariffPrice.comp) return;//when page wasn't loaded

    var template = "%i Р x %i шт = %i Р";
    //Comp count
    $scope.sComp = sprintf(template, 
      $scope.compUnitPrice,
      $scope.customerTariffOptions.comp,
      $scope.compUnitPrice * $scope.customerTariffOptions.comp);
    //Org count
    $scope.sOrg = sprintf(template, 
      $scope.currentTariffPrice.org,
      $scope.customerTariffOptions.org,
      $scope.currentTariffPrice.org * $scope.customerTariffOptions.org);
    //Soho count
    $scope.sSoho = sprintf(template,
      $scope.currentTariffPrice.soho,
      $scope.customerTariffOptions.soho,
      $scope.currentTariffPrice.soho * $scope.customerTariffOptions.soho);
    //WinServer count
    $scope.sWinServer = sprintf(template,
      $scope.currentTariffPrice.winServer,
      $scope.customerTariffOptions.winServer,
      $scope.currentTariffPrice.winServer * $scope.customerTariffOptions.winServer);
    //MiniATS count
    $scope.sMiniATS = sprintf(template, 
      $scope.currentTariffPrice.miniATS,
      $scope.customerTariffOptions.miniATS,
      $scope.currentTariffPrice.miniATS * $scope.customerTariffOptions.miniATS);
    //Local network
    $scope.sLocalNetwork = sprintf("%i Р (%s) = %i Р",
      $scope.currentTariffPrice.localNetwork,
      $scope.customerTariffOptions.localNetwork ? "да" : "нет",
      $scope.customerTariffOptions.localNetwork ? $scope.currentTariffPrice.localNetwork : 0);
  }


  //Calculate total price
  var getTotalPrice = function(){
    if (!$scope.currentTariffPrice.comp) return;//when page wasn't loaded

    var totalPrice = 0;
    //Comp total price
    if ($scope.customerTariffOptions.comp >=1 && $scope.customerTariffOptions.comp <=5)
      totalPrice += $scope.customerTariffOptions.comp * $scope.currentTariffPrice.comp.comp_1_5;
    else if ($scope.customerTariffOptions.comp >=6 && $scope.customerTariffOptions.comp <=10)
      totalPrice += $scope.customerTariffOptions.comp * $scope.currentTariffPrice.comp.comp_6_10;
    else if ($scope.customerTariffOptions.comp >=11 && $scope.customerTariffOptions.comp <=15)
      totalPrice += $scope.customerTariffOptions.comp * $scope.currentTariffPrice.comp.comp_11_15;
    else if ($scope.customerTariffOptions.comp >=16 && $scope.customerTariffOptions.comp <=24)
      totalPrice += $scope.customerTariffOptions.comp * $scope.currentTariffPrice.comp.comp_16_24;
    else if ($scope.customerTariffOptions.comp >=25 && $scope.customerTariffOptions.comp <=40)
      totalPrice += $scope.customerTariffOptions.comp * $scope.currentTariffPrice.comp.comp_25_40;
    
    //The rest total
    totalPrice += 
      $scope.customerTariffOptions.org * $scope.currentTariffPrice.org + 
      $scope.customerTariffOptions.soho * $scope.currentTariffPrice.soho + 
      $scope.customerTariffOptions.winServer * $scope.currentTariffPrice.winServer + 
      $scope.customerTariffOptions.miniATS * $scope.currentTariffPrice.miniATS + 
      ($scope.customerTariffOptions.localNetwork ? $scope.currentTariffPrice.localNetwork : 0);
    
    return totalPrice;
  }

  //Apply price components to view (when user drags sliders, for example)
  var updateView = function(){
    setTariffPriceComponents();

    //$scope.$apply();
  }



  $scope.changePersonStatus = function(status){
    $scope.newOrderData.status = status;
  }


  //Order consultation with phone number
  $scope.orderMinInfo = function(){
    if ($scope.formMinInfo.$valid)
    {
      var msg = "Была добавлена новая заявка. Телефон клиента: " + $scope.newOrderData.phone;

      Mailer.sendEmail(msg).success(function(){
        $().toastmessage('showToast', {
          text: 'Спасибо, Ваша заявка отправлена!',
          sticky: false,
          closeText: "ЗАКРЫТЬ",
          type: 'notice', 
          stayTime: 5000,
          position: 'middle-center'
        });
        //dialogSentEmail.toggle.bind(dialogSentEmail)();

        console.log("Email was sent");
      });
    }
  }
  //Order consultation with full information about client
  $scope.orderFullInfo = function(){
    if ($scope.formFullInfo.$valid)
    {
      var msg = "Была добавлена новая заявка. Информация о клиенте: <br><br>" + 
        ($scope.newOrderData.fio ? "ФИО: " + $scope.newOrderData.fio + "<br>" : '') + 
        "Статус: " + ($scope.newOrderData.status == 'private' ? 'частное лицо' : 
          ($scope.newOrderData.status == 'legal' ? 'представляю организацию' : '')) + "<br>" + 
        ($scope.newOrderData.email ? "E-mail: " + $scope.newOrderData.email + "<br>" : '') + 
        ($scope.newOrderData.phone ? "Тел.: " + $scope.newOrderData.phone + "<br>" : '') + 
        ($scope.newOrderData.msg ? "Сообщение: <br>" + $scope.newOrderData.msg : '');

      Mailer.sendEmail(msg).success(function(){
        $().toastmessage('showToast', {
          text: 'Спасибо, Ваша заявка отправлена!',
          sticky: false,
          closeText: "ЗАКРЫТЬ",
          type: 'notice', 
          stayTime: 5000,
          position: 'middle-center'
        });
        // dialogSentEmail.toggle.bind(dialogSentEmail)();

        console.log("Email was sent");
      });
    }
  }
  //Order consultation with phone number and tariff options
  $scope.orderTariff = function(){
    if ($scope.formTariff.$valid)
    {
      var msg = "Была добавлена новая заявка. Телефон клиента: " + $scope.newOrderData.phone + ".<br>" + 
        "Выбранный тариф: " + 
          ($scope.currentTariffPrice == $scope.baseTariffPrice ? "начальный." : 
            ($scope.currentTariffPrice == $scope.standardTariffPrice ? "стандартный." : "")) + "<br><br>";
      msg += "Компьютерная техника: " + $scope.customerTariffOptions.comp + " шт. " + 
        "(" + $scope.compUnitPrice +  " руб. за единицу) <br>";
      msg += "Оргтехника: " + $scope.customerTariffOptions.org + "шт. " + 
        "(" + $scope.currentTariffPrice.org +  " руб. за единицу) <br>";
      msg += "Маршрутизатор уровня SOHO: " + $scope.customerTariffOptions.soho + "шт. " + 
        "(" + $scope.currentTariffPrice.soho +  " руб. за единицу) <br>";
      msg += "Сервер на базе windows Server: " + $scope.customerTariffOptions.winServer + " шт. " + 
        "(" + $scope.currentTariffPrice.winServer +  " руб. за единицу) <br>";
      msg += "Мини АТС: " + $scope.customerTariffOptions.miniATS + " шт. " + 
        "(" + $scope.currentTariffPrice.miniATS +  " руб. за единицу) <br>";
      msg += "Локальная сеть: " + ($scope.customerTariffOptions.localNetwork ? "да" : "нет") + 
        " (" + $scope.currentTariffPrice.localNetwork + " руб.) <br>";
      msg += "<br>Итого: " + $scope.totalPrice + " руб. "
      
      Mailer.sendEmail(msg).success(function(){
        $().toastmessage('showToast', {
          text: 'Спасибо, Ваша заявка отправлена!',
          sticky: false,
          closeText: "ЗАКРЫТЬ",
          type: 'notice', 
          stayTime: 5000,
          position: 'middle-center'
        });
        // dialogSentEmail.toggle.bind(dialogSentEmail)();
        
        console.log("Email was sent");
      });
    }
  }


});
